LDFLAGS=
override CFLAGS:= -MMD -Wall -std=c99 -pedantic -O2 $(CFLAGS)
EXEC=daemonizer
CC=gcc

SOURCES=main.c
OBJECTS=$(SOURCES:.c=.o)
DEPENDS=$(OBJECTS:.o=.d)

.PHONY: all clean

all: $(SOURCES) $(EXEC)

$(EXEC): $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@ 

-include $(DEPENDS)

clean:
	rm $(OBJECTS) $(EXEC) $(DEPENDS)
