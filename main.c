#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdarg.h>

int main(int argc, char** argv) {
    char* to_daemonize = NULL;
    if (argc == 2) {
        // TODO: need full path here,
        // because the child won't have PATH set properly
        to_daemonize = argv[1];
        printf("to_daemonize: %s\n", to_daemonize);
    } else {
        exit(EXIT_FAILURE);
    }

    pid_t child_pid = fork();
    if (child_pid < 0) {
        exit(EXIT_FAILURE);
    } else if (child_pid > 0) {
        exit(EXIT_SUCCESS);
    }
    
    // child begins execution here
    
    umask(0); // set umask so that we can access our own files
    // Log file TODO
    
    pid_t sid = setsid(); // create session ID for child process
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    // change cwd to root, which is guaranteed to exist
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    // close standard streams because they're useless
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
   
    if ((execlp(to_daemonize, to_daemonize, NULL)) == -1) {
        exit(EXIT_FAILURE);
    }
    
    return 0;
}
